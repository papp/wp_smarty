<?
class wp_smarty__class__library extends wp_smarty__class__library__parent
{
	function __construct()
	{
		parent::{__function__}();
		include_once('tmp/system/library/smarty/Smarty.class.php');
		
		#$this->C->setting()->get_setting();
		/*if(!$this->D['SETTING']['D']['GLOBAL']['D']['SMARTY']['D'])
		{
			$this->D['SETTING']['D']['GLOBAL']['D']['SMARTY']['D'] = array(
				'DEBUGGING'			=> array( 'TYPE' => 'checkbox', 'VALUE' => 0 ),
				'CACHING'			=> array( 'TYPE' => 'checkbox', 'VALUE' => 0 ),
				'CACHE_LIVETIME'	=> array( 'VALUE' => 120 ),
				'TEMPLATE_DIR'		=> array( 'VALUE' => 'tmp/system/data/' ),
				'COMPILE_DIR'		=> array( 'VALUE' => 'tmp/' ),
			);
			$this->C->setting()->set_setting($D);
		}*/
		#$SMARTY = $this->D['SETTING']['D']['GLOBAL']['D']['SMARTY']['D'];

		if(!$this->D['CONFIG']['MODUL']['D']['wp_smarty']['SETTING']['D']['GLOBAL']) #Standarteinstellungen laden falls nicht vorhanden
		{
			$d = $this->C->modul()->getModulInfo('smarty');
			$this->D['CONFIG']['MODUL']['D']['wp_smarty']['SETTING']['D']['GLOBAL'] = $d['SETTING']['D']['GLOBAL'];
		}
		$SMARTY = $this->D['CONFIG']['MODUL']['D']['wp_smarty']['SETTING']['D']['GLOBAL'];
		$this->smarty = new Smarty();
		$this->smarty->debugging = $SMARTY['DEBUGGING']['VALUE'];
		$this->smarty->caching = $SMARTY['CACHING']['VALUE'];
		$this->smarty->cache_lifetime = $SMARTY['CACHE_LIVETIME']['VALUE'];
		$this->smarty->template_dir = $SMARTY['TEMPLATE_DIR']['VALUE'];
		$this->smarty->compile_dir = $SMARTY['COMPILE_DIR']['VALUE'];
		$this->smarty->error_reporting = ~E_NOTICE;
		

		$this->smarty->loadPlugin('smarty_compiler_switch');
		
		#$this->smarty->assign('D',$D);
	}
	
	function smarty($d = null)
	{
		# include_once(__dir__.'/../data/library/smarty/Smarty.class.php');
		# $this->smarty = new Smarty();
		parent::{__function__}($d);
		return $this->smarty;
	}

} 