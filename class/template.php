<?
class wp_smarty__class__template extends wp_smarty__class__template__parent
{
	function __construct(&$D = null)
	{
        parent::{__function__}($D);
	}

	function display($d=null)
	{
		$this->C->library()->smarty()->assign('D', $this->D);
		krsort($this->D['TEMPLATE']['FILE']);
		$file = implode('|',$this->D['TEMPLATE']['FILE']);
		$this->D['TEMPLATE']['HTML'] = $this->C->library()->smarty()->fetch('extends:'.$file);
		parent::{__function__}($d);
		exit($this->D['TEMPLATE']['HTML']);
	}
	
}